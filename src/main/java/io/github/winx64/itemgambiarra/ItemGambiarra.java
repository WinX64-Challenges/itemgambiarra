package io.github.winx64.itemgambiarra;

import org.bukkit.Bukkit;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

public final class ItemGambiarra extends JavaPlugin implements Listener {

	private static final String ITEM_KEY = "saved-item";

	@Override
	public void onEnable() {
		ConfigurationSerialization.registerClass(GambiarraStack.class);

		Bukkit.getPluginManager().registerEvents(this, this);

		this.saveDefaultConfig();
	}

	@EventHandler
	public void onQuit(PlayerQuitEvent event) {
		getConfig().set(ITEM_KEY, new GambiarraStack(event.getPlayer().getItemInHand()));
		saveConfig();
	}
}
