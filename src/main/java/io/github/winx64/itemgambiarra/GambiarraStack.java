package io.github.winx64.itemgambiarra;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.SerializableAs;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

@SerializableAs("GambiarraStack")
public final class GambiarraStack implements ConfigurationSerializable {

	private static final String ID_KEY = "id";
	private static final String DATA_KEY = "data";
	private static final String AMOUNT_KEY = "amount";
	private static final String NAME_KEY = "nome";
	private static final String LORE_KEY = "lore";

	private final ItemStack handle;

	public GambiarraStack(ItemStack handle) {
		this.handle = handle;
	}

	@SuppressWarnings("deprecation")
	public GambiarraStack(Map<String, Object> map) {
		Object idObject = map.get(ID_KEY);
		if (idObject == null || !(idObject instanceof Number)) {
			throw new IllegalArgumentException("id cannot be null and must be a number");
		}
		int id = ((Number) idObject).intValue();

		Object dataObject = map.get(DATA_KEY);
		if (dataObject == null || !(dataObject instanceof Number)) {
			throw new IllegalArgumentException("data cannot be null and must be a number");
		}
		byte data = ((Number) dataObject).byteValue();

		Object amountObject = map.get(AMOUNT_KEY);
		if (amountObject == null || !(amountObject instanceof Number)) {
			throw new IllegalArgumentException("amount cannot be null and must be a number");
		}
		short amount = ((Number) amountObject).shortValue();

		Object nameObject = map.get(NAME_KEY);
		String name = null;
		if (nameObject != null) {
			if (!(nameObject instanceof String)) {
				throw new IllegalArgumentException("name must be a String");
			}

			name = (String) nameObject;
		}

		Object loreObject = map.get(LORE_KEY);
		List<String> lore = null;
		if (loreObject != null) {
			if (!(loreObject instanceof List)) {
				throw new IllegalArgumentException("lore must be a list of Strings");
			}

			List<?> rawLore = (List<?>) loreObject;
			lore = new ArrayList<String>();
			for (Object loreEntryObject : rawLore) {
				if (!(loreEntryObject instanceof String)) {
					throw new IllegalArgumentException("lore must be a list of Strings");
				}

				lore.add((String) loreEntryObject);
			}
		}

		ItemStack item = new ItemStack(id, amount, (short) 0, data);
		ItemMeta meta = item.getItemMeta();

		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
		meta.setLore(lore);
		item.setItemMeta(meta);

		this.handle = item;
	}

	@SuppressWarnings("deprecation")
	public Map<String, Object> serialize() {
		Map<String, Object> map = new LinkedHashMap<String, Object>();
		ItemMeta meta = handle.getItemMeta();

		map.put(ID_KEY, handle.getTypeId());
		map.put(DATA_KEY, handle.getData().getData());
		map.put(AMOUNT_KEY, handle.getAmount());

		if (meta != null) {
			if (meta.hasDisplayName()) {
				map.put(NAME_KEY, meta.getDisplayName().replace(ChatColor.COLOR_CHAR, '&'));
			}
			if (meta.hasLore()) {
				map.put(LORE_KEY, meta.getLore());
			}
		}

		return map;
	}

	public static GambiarraStack deserialize(Map<String, Object> map) {
		return new GambiarraStack(map);
	}

	public ItemStack getHandle() {
		return handle.clone();
	}
}
